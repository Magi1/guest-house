**Guest House**

Here is a front end and a back end project for a guest house web application using mongodb, express, react and node

Before starting with any process make shure that you have install and setup mongodb on localhost [Install MongoDB Community Edition](https://docs.mongodb.com/v3.2/administration/install-community/)

after setingup mongodb as a servise and making sure you have node installed in your computer by 
typing node -v in the terminal if you don't have it go to node website and [install NodeJS](https://nodejs.org) 
after sething up go to the back end folder and enter the following in the terminal

npm install

npm start


then the next step is to go to the front end folder and enter the following in the terminal without closing the first terminal

npm install

npm start