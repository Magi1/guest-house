const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const guestRoutes = express.Router();
const PORT = 4000;
let Guest = require('./guest.model'); 
app.use(cors());
app.use(bodyParser.json());
mongoose.connect('mongodb://127.0.0.1:27017/guests', { useNewUrlParser: true,useUnifiedTopology:true });
const connection = mongoose.connection;
connection.once('open', function () {
    console.log("MongoDB database connection established successfully");
})
guestRoutes.route('/').get(function (req, res) {
    Guest.find(function (err, guests) {
        if (err) {
            console.log(err);
        } else {
            res.json(guests);
        }
    });
});
guestRoutes.route('/:id').get(function (req, res) {
    let id = req.params.id;
    Guest.findById(id, function (err, guest) {
        res.json(guest);
    });
});
guestRoutes.route('/update/:id').post(function (req, res) {
    Guest.findById(req.params.id, function (err, guest) {
        if (!guest)
            res.status(404).send("data is not found");
        else
            guest.guest_fullName = req.body.guest_fullName;
        guest.guest_city = req.body.guest_city;
        guest.guest_city = req.body.guest_city;
        guest.guest_subCity = req.body.guest_subCity;
        guest.guest_woreda = req.body.guest_woreda;
        guest.guest_houseNumber = req.body.guest_houseNumber;
        guest.guest_phoneNumber = req.body.guest_phoneNumber;
        guest.guest_ID_PassportNumber = req.body.guest_ID_PassportNumber;
        guest.guest_purposeOfStay = req.body.guest_purposeOfStay;
        guest.save().then(guest => {
            res.json('Guest updated!');
        })
            .catch(err => {
                res.status(400).send("Update not possible");
            });
    });
});
guestRoutes.route('/add').post(function (req, res) {
    let guest = new Guest(req.body);
    guest.save()
        .then(guest => {
            res.status(200).json({ 'guest': 'guest added successfully' });
        })
        .catch(err => {
            res.status(400).send('adding new guest failed');
        });
});
app.use('/guests', guestRoutes);
app.listen(PORT, function () {
    console.log("Server is running on Port: " + PORT);
});