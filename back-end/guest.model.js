const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let Guest = new Schema({
    guest_fullName: {
        type: String
    },
    guest_city: {
        type: String
    },
    guest_subCity: {
        type: String
    },
    guest_woreda: {
        type: String
    },
    guest_houseNumber: {
        type: String
    },
    guest_phoneNumber: {
        type: String
    },
    guest_ID_PassportNumber: {
        type: String
    },
    guest_purposeOfStay: {
        type: String
    }
});
module.exports = mongoose.model('Guest', Guest);