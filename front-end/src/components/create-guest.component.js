import React, { Component } from 'react';
import axios from 'axios'

export default class GuestEntry extends Component {

    constructor(props) {
        super(props);

        this.onChangeGuestFullName = this.onChangeGuestFullName.bind(this);
        this.onChangeGuestCity = this.onChangeGuestCity.bind(this);
        this.onChangeGuestSubCity = this.onChangeGuestSubCity.bind(this);
        this.onChangeGuestWoreda = this.onChangeGuestWoreda.bind(this);
        this.onChangeGuestHouseNumber = this.onChangeGuestHouseNumber.bind(this);
        this.onChangeGuestPhoneNumber = this.onChangeGuestPhoneNumber.bind(this);
        this.onChangeGuestID_PassportNumber = this.onChangeGuestID_PassportNumber.bind(this);
        this.onChangeGuestPurposeOfStay = this.onChangeGuestPurposeOfStay.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            guest_fullName:'',
            guest_city: '',
            guest_subCity: '',
            guest_woreda: '',
            guest_houseNumber: '',
            guest_phoneNumber: '',
            guest_ID_PassportNumber: '',
            guest_purposeOfStay: ''
        }
    }

    onChangeGuestFullName(e) {
        this.setState({
            guest_fullName: e.target.value
        });
    }

    onChangeGuestCity(e) {
        this.setState({
            guest_city: e.target.value
        });
    }

    onChangeGuestSubCity(e) {
        this.setState({
            guest_subCity: e.target.value
        });
    }
    onChangeGuestWoreda(e) {
        this.setState({
            guest_woreda: e.target.value
        });
    }
    onChangeGuestHouseNumber(e) {
        this.setState({
            guest_houseNumber: e.target.value
        });
    }
    onChangeGuestPhoneNumber(e) {
        this.setState({
            guest_phoneNumber: e.target.value
        });
    }
    onChangeGuestID_PassportNumber(e) {
        this.setState({
            guest_ID_PassportNumber: e.target.value
        });
    }
    onChangeGuestPurposeOfStay(e) {
        this.setState({
            guest_purposeOfStay: e.target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();

        console.log(`Form submitted:`);
        console.log(`Guest FullName: ${this.state.guest_fullName}`);

        const newGuest = {
            guest_fullName: this.state.guest_fullName,
            guest_city: this.state.guest_city,
            guest_subCity: this.state.guest_subCity,
            guest_woreda: this.state.guest_woreda,
            guest_houseNumber: this.state.guest_houseNumber,
            guest_phoneNumber: this.state.guest_phoneNumber,
            guest_ID_PassportNumber: this.state.guest_ID_PassportNumber,
            guest_purposeOfStay: this.state.guest_purposeOfStay
        };

        axios.post('http://localhost:4000/guests/add', newGuest)
            .then(res => console.log(res.data));

        this.setState({
            guest_fullName: '',
            guest_city: '',
            guest_subCity: '',
            guest_woreda: '',
            guest_houseNumber: '',
            guest_phoneNumber: '',
            guest_ID_PassportNumber: '',
            guest_purposeOfStay: ''
        })
    }

    render() {
        return (
            <div style={{ marginTop: 10 }}>
                <h3>Create New Guest</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Full Name: </label>
                        <input type="text"
                            className="form-control"
                            value={this.state.guest_fullName}
                            onChange={this.onChangeGuestFullName}
                        />
                    </div>
                    <div className="form-group">
                        <label>City: </label>
                        <input
                            type="text"
                            className="form-control"
                            value={this.state.guest_city}
                            onChange={this.onChangeGuestCity}
                        />
                    </div>
                    <div className="form-group">
                        <label>Sub City: </label>
                        <input type="text"
                            className="form-control"
                            value={this.state.guest_subCity}
                            onChange={this.onChangeGuestSubCity}
                        />
                    </div>
                    <div className="form-group">
                        <label >Woreda: </label>
                        <input 
                            type="text"
                            className="form-control"
                            value={this.state.guest_woreda}
                            onChange={this.onChangeGuestWoreda}
                        />
                    </div>
                    <div className="form-group">
                        <label>House Number: </label>
                        <input type="text"
                            className="form-control"
                            value={this.state.guest_houseNumber}
                            onChange={this.onChangeGuestHouseNumber}
                        />
                    </div>
                    <div className="form-group">
                        <label>Phone Number: </label>
                        <input
                            type="text"
                            className="form-control"
                            value={this.state.guest_phoneNumber}
                            onChange={this.onChangeGuestPhoneNumber}
                        />
                    </div>
                    <div className="form-group">
                        <label>ID/ Passport Number: </label>
                        <input type="text"
                            className="form-control"
                            value={this.state.guest_ID_PassportNumber}
                            onChange={this.onChangeGuestID_PassportNumber}
                        />
                    </div>
                    <div className="form-group">
                        <label>Purpose of Stay: </label>
                        <input
                            type="text"
                            className="form-control"
                            value={this.state.guest_purposeOfStay}
                            onChange={this.onChangeGuestPurposeOfStay}
                        />
                    </div>
                    

                    <div className="form-group">
                        <input type="submit" value="Create Guest" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        )
    }
}