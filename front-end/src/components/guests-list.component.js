import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Guest = props => (
    <tr>
        <td >{props.guest.guest_fullName}</td>
        <td >{props.guest.guest_city}</td>
        {/* <td >{props.guest.guest_subCity}</td>
        <td >{props.guest.guest_woreda}</td>
        <td >{props.guest.guest_houseNumber}</td> */}
        <td >{props.guest.guest_phoneNumber}</td>
        <td >{props.guest.guest_ID_PassportNumber}</td>
        <td >{props.guest.guest_purposeOfStay}</td>
        <td>
            <Link to={"/detail/" + props.guest._id}>Detail</Link>
        </td>
    </tr>
)

export default class GuestList extends Component {

    constructor(props) {
        super(props);
        this.state = { guests: [] };
    }

    componentDidMount() {
        axios.get('http://localhost:4000/guests/')
            .then(response => {
                this.setState({ guests: response.data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    guestList() {
        return this.state.guests.map(function (currentGuest, i) {
            return <Guest guest={currentGuest} key={i} />;
        })
    }

    render() {
        return (
            <div>
                <h3>Guests List</h3>
                <table className="table table-striped" style={{ marginTop: 20 }} >
                    <thead>
                        <tr>
                            <th>Full Name</th>
                            <th>City</th>
                            <th>Phone Number</th>
                            <th>ID/ Passport Number</th>
                            <th>Purpose of Stay</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.guestList()}
                    </tbody>
                </table>
            </div>
        )
    }
}