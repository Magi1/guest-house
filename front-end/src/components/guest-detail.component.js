import React, { Component } from 'react';
import axios from 'axios';

export default class GuestDetail extends Component {

    constructor(props) {
        super(props);

        this.state = {
            guest_fullName: '',
            guest_city: '',
            guest_subCity: '',
            guest_woreda: '',
            guest_houseNumber: '',
            guest_phoneNumber: '',
            guest_ID_PassportNumber: '',
            guest_purposeOfStay: ''
        }
    }

    componentDidMount() {
        axios.get('http://localhost:4000/guests/' + this.props.match.params.id)
            .then(response => {
                this.setState({
                    guest_fullName: response.data.guest_fullName,
                    guest_city: response.data.guest_city,
                    guest_subCity: response.data.guest_subCity,
                    guest_woreda: response.data.guest_woreda,
                    guest_houseNumber: response.data.guest_houseNumber,
                    guest_phoneNumber: response.data.guest_phoneNumber,
                    guest_ID_PassportNumber: response.data.guest_ID_PassportNumber,
                    guest_purposeOfStay: response.data.guest_purposeOfStay
                }); 
            })
            .catch(function (error) {
                console.log(error)
            })
            
    }

    render() {
        return (
            <div>
                <h4>Guest Detail</h4>
                <div className="container">

                    <table className="table table-striped" style={{ marginTop: 20 }} >
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><h5>Full Name: </h5></td>
                                <td><h4  >{this.state.guest_fullName}</h4></td>
                            </tr>
                            <tr>
                                <td> <h5>City: </h5></td>
                                <td><h4  >{this.state.guest_city}</h4></td>
                            </tr>
                            <tr>
                                <td><h5>Sub City: </h5></td>
                                <td> <h4  >{this.state.guest_subCity}</h4></td>
                            </tr>
                            <tr>
                                <td><h5>Woreda: </h5></td>
                                <td><h5>{this.state.guest_woreda}</h5></td>
                            </tr>
                            <tr>
                                <td> <h5>House Number: </h5></td>
                                <td><h4  >{this.state.guest_houseNumber}</h4></td>
                            </tr>
                            <tr>
                                <td><h5>Phone Number: </h5></td>
                                <td><h4  >{this.state.guest_phoneNumber}</h4></td>
                            </tr>
                            <tr>
                                <td><h5>ID/ Passport Number: </h5></td>
                                <td><h4  >{this.state.guest_ID_PassportNumber}</h4></td>
                            </tr>
                            <tr>
                                <td><h5>Purpose of Stay: </h5></td>
                                <td><h4  >{this.state.guest_purposeOfStay}</h4></td>
                            </tr>
                        </tbody>
                    </table>
                    
                </div>
                
                    
                    
            </div>
        )
    }
}