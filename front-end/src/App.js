import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";

import GuestEntry from "./components/create-guest.component";
import GuestDetail from "./components/guest-detail.component";
import GuestsList from "./components/guests-list.component";

import logo from "./logo.svg";

class App extends Component {
  render() {
    return (
      <Router>
        <div className="container">
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="https://reactjs.org/" target="_blank" >
              <img src={logo} width="30" height="30" alt="React js" />
            </a>
            <Link to="/" className="navbar-brand">Guest House</Link>
            <div className="collapse navbar-collapse">
              <ul className="navbar-nav mr-auto">
                <li className="navbar-item">
                  <Link to="/" className="nav-link">Guests</Link>
                </li>
                <li className="navbar-item">
                  <Link to="/create" className="nav-link">Guest Entry</Link>
                </li>
              </ul>
            </div>
          </nav>
          <br />
          <Route path="/" exact component={GuestsList} />
          <Route path="/detail/:id" component={GuestDetail} />
          <Route path="/create" component={GuestEntry} />
        </div>
      </Router>
    );
  }
}

export default App;